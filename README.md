# Docker-compose setup for exerimenting with marathon and mesos

Docker-compose setup which can be used to experiment with marathon/mesos. Since I had a hard time setting up marathon, I decided to make it public so people can experiment with it in just 1 go

## ports

Mesos: 5050
Marathon: 8080
Zookeeper gui: 9090

## Links:
https://mesosphere.github.io/marathon/
http://mesos.apache.org/
http://zookeeper.apache.org/
